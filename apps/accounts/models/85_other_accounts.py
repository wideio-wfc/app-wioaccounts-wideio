#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *

# FIXME: THIS SHOULD NOT BE IN THIS APP

socialaccount_types = {
    'twitter': {
        "name": "Twitter"
    },
    'linkedin': {
        "name": "LinkedIn"
    },
    'google': {
        "name": "Google Plus"
    },
    'DBLP': {
        "name": "DBLP"
    }
}

@wideio_publishable()
@wideio_owned()
@wideiomodel
class SocialAccount(models.Model):
    account_type = models.CharField(
        max_length=64,
        choices=map(lambda (k, o): (k, o['name']), socialaccount_types.items()))
    account_identifier = models.TextField(
        max_length=64,
        help_text='identifier used on that service')
    account_url = models.URLField(
        max_length=128,
        help_text='Your URL on that system.')

    def get_all_references(self, stack):
        st = []
        if self.issuer not in stack:
            st.append(self.issuer)
        if self.owner not in stack:
            st.append(self.owner)
        return st

    def get_network_icon(self):
        return "/media/images/socialaccounts/"+self.account_type+'.png'

    def on_add(self, r):
        r.user.get_extended_profile().increase_kudos(250)
        return {'_redirect':'/accounts/profile/'}

    def on_delete(self, r):
        r.user.get_extended_profile().decrease_kudos(250)
        return {'_redirect':'/accounts/profile/'}

    def can_view(self, request):
        return True

    @staticmethod
    def can_list(request):
        return request.user.is_superuser

    def on_view(self, r):
        return {'_redirect': self.owner.get_view_url()}

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        NO_DRAFT = True
        CAN_TRANSFER=False
        DISABLE_VIEW_ACCOUNTING = True
        icon = 'ion-link'
        mandatory_fields = ['account_type', 'account_identifier', 'account_url']
        class Actions:
            @wideio_action(icon="icon-cogs",
                           possible=lambda o, r: r.user == o.owner,
                           xattrs=' title="Import profile information from this account" ')
            def update_from(self, request):
                user = self.owner
                if self.account_type == 'linkedin':
                    from network.socialaccounts.SAlinked import LinkedInAccount
                    # LinkedInAccount.on_sync(user, self)
                elif self.account_type == 'twitter':
                    # from network.socialaccounts.SAtwitter import TwitterAccount
                    # TwitterAccount.on_sync(user, self)
                    pass
                user.save()
                return {"_redirect": "/accounts/profile/"}
        MOCKS={
            'default':[
                {
                    'account_type': 'twitter',
                    'account_identifier': 'john-smith',
                    'account_url': 'http://www.twitter.com/john-smith/'
                }
            ]
        }

    @staticmethod
    def can_list(request):
        return request.user.is_staff

    def get_icon(arg=None):
        if type(arg) == SocialAccount:
            self = arg
            return ('ion-social-' + self.account_type) if self.account_type else "ion-link"
        else:
            return 'ion-link'
