#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *

# FIXME: THIS SHOULD NOT BE IN THIS APP

@wideio_publishable()
@wideio_owned()
@wideiomodel
class AcademicDegree(models.Model):
    title = models.TextField( max_length=64, help_text='Title of the degree (e.g. PhD, BA, etc...)')
    grade = models.TextField( blank=True, max_length=16, help_text='Grade of the degree (e.g. 1:1, 2:1 Hons, etc...)')
    subject = models.TextField(blank=True, max_length=256, help_text='Degree subject')
    issuer = models.ForeignKey('Organisation', null=True, help_text='Place you obtained the degree', db_index=True)
    year = models.IntegerField(blank=True)  # ,minvalue=1920)

    def get_all_references(self, stack):
        st = []
        if self.issuer not in stack:
            st.append(self.issuer)
        if self.owner not in stack:
            st.append(self.owner)
        return st

    def on_add(self, r):
        r.user.get_extended_profile().increase_kudos(250)
        return {'_redirect':'/accounts/profile/'}

    def on_delete(self, r):
        r.user.get_extended_profile().decrease_kudos(250)
        return {'_redirect':'/accounts/profile/'}

    def can_view(self, request):
        return True

    def on_view(self, r):
        return {'_redirect': self.owner.get_view_url()}

    @staticmethod
    def can_list(request):
        return request.user.is_staff

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        NO_DRAFT = True
        CAN_TRANSFER=False
        DISABLE_VIEW_ACCOUNTING = True
        icon = "ion-ribbon-a"
        MOCKS={
            'default': [
                dict(
                    title = "BA",
                    grade = "1:1",
                    subject ="Physics",
                    issuer = "Camford University",
                    year=2013
                )
            ]
        }
