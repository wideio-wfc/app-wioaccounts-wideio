#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *

@wideio_publishable()
@wideiomodel
class ScientistProfile(models.Model):
    user_profile = (
        with_app_validate(lambda v, s: True)(
            with_db_validate(lambda v, s: True)
            (models.OneToOneField('UserAccount', related_name="scientific_profile"))
        )
    )

    # biography = models.TextField(blank=True, max_length=1000, help_text='(Self introduction) e.g. computer scientist')
    research = models.TextField(
                blank=True,
                max_length=2000,
                help_text='e.g. finding efficient algorithms for discrete rotations in cellular automata')  # < List of keywords..

    # Team you are normally working with
    team = models.ForeignKey(
        'Team',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        db_index=True,
        help_text="")

    academic_rank = models.CharField(max_length=10,
                                     choices=(("NM", "New member"), ("ND", ""),
                                              ("BSc", "Bachelor of Science"), ("BO", "other Bachelors' degree"),
                                              ("MSc", "Master of Science"), ("MO", "other Masters' degree"),
                                              ("EngD", "Doctor of Engineering"), ("PhD", "PhD"),
                                              ("DO", "Other Doctors' degree"),
                                              ("EProf", "Emeritus Professor"), ("Prof", "Professor"),
                                              ("AProf", "Associate Professor"), ("A2Prof", "Assistant Professor"),
                                              ("RA", "Research Associate"), ("LE", "Lecture"),
                                              ("ENG", "Engineering Degree"),
                                              ),
                                     db_index=True
                                     )

    @property
    def organisation(self):
        from accounts.models import UserAccount
        return UserAccount.objects.get(scientific_profile_id=self.id).organisation

    def get_all_references(self, stack):
        st = []
        if self.default_team not in stack:
            st.append(self.default_team)
        if self.default_origanization not in stack:
            st.append(self.default_organization)
        return st

    ##
    # basic permissions
    ##
    def can_view(self, request):
        return True

    def can_update(self, request):
        return request.user.get_scientific_profile().id == self.id

    def can_delete(self, request):
        return self.is_draft and self.can_update(request)

    @staticmethod
    def can_list(request):
        return request.user.is_staff

    @staticmethod
    def can_add(request):
        return False

    def can_update(self, request):
        return request.user.is_staff or (request.user == self.user_profile)

    @staticmethod
    def _on_load_postprocess(M):
        def is_scientist(self):
            return ScientistProfile.objects.filter(user_profile=self).count() != 0


        def is_scientist_setter(self, value):
            if value and not self.is_scientist:
                sp = ScientistProfile()
                sp.user_profile = self
                sp.save()
            if not value and self.is_scientist:
                return ScientistProfile.objects.filter(user_profile=self).delete()

        def get_scientific_profile(self):
            return self.scientific_profile

        def set_scientist(self):
            useraccount.WIDEIO_Meta.Actions.set_scientist.of(self, None)

        useraccount = filter(lambda m: m.__name__ == 'UserAccount', M)[0]
        useraccount.is_scientist = property(is_scientist, is_scientist_setter)
        useraccount.get_scientific_profile = get_scientific_profile
        useraccount.set_scientist = set_scientist

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only
        form_exclude = ['user_profile']
        icon = "icon-superscript"
        CAN_TRANSFER = False
        MOCKS = {
            'default': [
                {
                    '@id': 'user0000_scientific_profile',
                    'user_profile': 'user0000',
                    'research': 'Operational Research',
                    'team': None,
                    'academic_rank': 'NM'
                }
            ]
        }
