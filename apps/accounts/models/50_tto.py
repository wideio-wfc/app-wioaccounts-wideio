#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *


# FIXME: THIS SHOULD NOT BE IN THIS APP
# FIXME: THIS ADDS A DEPENDENCY TO SOCIALNET

@wideio_publishable()
@wideio_owned()
@wideiomodel
class TTO(models.Model):
    """
    A technology - transfer office
    """
    verbose_name = "technology transfert office"
    API = True
    name = models.CharField(
        max_length=64,
        unique=True,
        blank=False,
        help_text="The name of the technology transfer office")

    admins = models.ManyToManyField(
        'accounts.UserAccount',
        related_name="tto_admin_of",
        blank=True)

    organisation = models.OneToOneField(
        'accounts.Organisation',
        related_name="tto_set",
    )

    members = models.ManyToManyField(
        'accounts.UserAccount',
        related_name="tto_member_of",
        blank=True)

    image = models.ForeignKey(
        'references.Image',
        null=False,
        help_text="An image used to identify your team visually")

    website = models.URLField(
        null=True,
        blank=True,
        help_text="A website if you have one")

    short_description = models.TextField()

    pseudo_user = models.ForeignKey('accounts.UserAccount', null=True, blank=True)

    questions = models.ManyToManyField('network.QNA')

    def on_add(self, request):
        self.members.add(request.user)
        self.admins.add(request.user)
        if self.pseudo_user == None:
            u = UserAccount()
            u.first_name = ""
            u.last_name = ""
            u.email_address = "team" + base64.b64encode(
                base64.b16decode(str(uuid.uuid1()).replace('-', '').upper())) + "@wide.io"
            u.username = u.email_address[:-8]
            u.is_active = False
            u.wiostate = 'A'
            u.metadata = {'team': self.id}
            u.save()
            self.pseudo_user = u
        self.save()

    def __unicode__(self):
        return unicode(self.name)

    def get_all_references(self):
        return []

    @staticmethod
    def can_list(request):
        return True

    def can_view(self, request):
        return True  # request.user in self.members.all()

    class WIDEIO_Meta:
        ADD_ENABLED = True
        icon = 'ion-ios-people'
        form_exclude = ['admins', 'members', 'pseudo_user', 'questions']
        # permissions = dec.perm_read_logged_users_write_for_staff_only
        permissions = dec.perm_for_logged_users_only
        sort_enabled = ['name']
        search_enabled = "name"

        class Actions:
            @wideio_action(icon="ion-email",
                           possible=lambda s,
                                           r: (r.user.is_authenticated() and r.user in s.members.all()))
            def send_message(self, request):
                from network.models import DirectMessage
                return HttpResponseRedirect(DirectMessage.get_add_url() + "?_AJAX=1&to=" + self.id)

        MOCKS = {
            "default": [
                {
                 "@id": "camford-tto",
                 "name": "Camford University Technology",
                 "admins": [{'@id': 'camford-admin'}],
                 "organisation": [{'@id': 'camford-university'}],
                 "members": [{'@id': 'camford-admin'}],
                 "image": [{'@id': 'camford-tto-logo'}],
                 "short_description": """
                 A fake tto that commecializes fake IP of a fake university used for tests purpose.
                 """
                }
            ]
        }
