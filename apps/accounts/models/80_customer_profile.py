#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *


@wideio_publishable()
@wideiomodel
class CustomerProfile(models.Model):
    professional_title = models.CharField(max_length=10, choices=(
        ("Exec Off", "Chief Executive Officer (CEO)"),
        ("Finance Off", "Chief Financial Officer (CFO)"),
        ("Tech Off", "Chief Technical Officer (CTO)"),
        ("Infra Off", "Chief Infrastructure Officer (CIO)"),
        ("Innov Off", "Chief Innovation Officer (CIO)"),
        ("Chairman", "Chairman"),
        ("President", "President"),
        ("Director", "Director"),
        ("Manager", "Division Manager"),
        ("T Leader", "Team Leader"),
        ("Software Eng", "Software Engineer"),
        ("Software Arc", "Software Architect"),
        ("System Eng", "System Engineer"),
        ("Test Eng", "Test Engineer"),
        ("Project Man", "Project Manager"),
        ("Consultant", "Consultant"),
        ("Researcher", "Researcher"),
        ("Programmer", "Programmer"),
        ("Journalist", "Journalist"),
        ("White hat", "White hat"),
        ("Teacher", "Teacher"),
        ("Other", "Other"),
    ), db_index=True)

    user_profile = models.OneToOneField('UserAccount', related_name="business_profile")

    def can_view(self, request):
        return True

    def can_update(self, request):
        return request.user.is_staff or (
            request.user.is_customer and request.user.business_profile.id == self.id)

    def can_delete(self, request):
        return False

    @staticmethod
    def can_list(request):
        return request.user.is_staff

    @staticmethod
    def can_add(request):
        return False

    def get_all_references(self):
        return []

    @staticmethod
    def _on_load_postprocess(M):
        def is_customer(self):
            return CustomerProfile.objects.filter(user_profile=self).count() != 0

        def is_customer_setter(self, value):
            if value and not self.is_customer:
                sp = CustomerProfile()
                sp.user_profile=self
                sp.save()
            if not value and self.is_customer:
                return CustomerProfile.objects.filter(user_profile=self).delete()

        def set_business(self):
            UserAccount.WIDEIO_Meta.Actions.set_business.of(self, None)

        def get_business_profile(self):
            return self.scientific_profile


        UserAccount = filter(lambda m: m.__name__ == 'UserAccount', M)[0]
        UserAccount.is_customer = property(is_customer, is_customer_setter)
        UserAccount.set_customer = set_business
        UserAccount.get_customer_profiler = get_business_profile

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only
        form_exclude = ['user_profile']
        icon = "icon-superscript"
        CAN_TRANSFER = False
        MOCKS = {
            'default': [
                {
                    '@id': 'user0000_scientific_profile',
                    'professional_title': 'Other',
                }
            ]
        }
