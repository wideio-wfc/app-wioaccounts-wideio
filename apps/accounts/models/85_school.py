#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *

# FIXME: THIS SHOULD NOT BE IN THIS APP
@wideio_publishable()
@wideio_owned()
@wideiomodel
class School(models.Model):

    """ Your attendance to a particular school """
    organisation = models.ForeignKey(
        'Organisation',
        null=True,
        help_text='Place you obtained the degree')
    cursus = models.TextField(
        blank=True,
        max_length=4000,
        help_text='Degree subject')
    from_date = models.DateTimeField()
    to_date = models.DateTimeField(blank=True)

    def get_all_references(self, stack):
        st = []
        if self.issuer not in stack:
            st.append(self.organisation)
        if self.owner not in stack:
            st.append(self.owner)
        return st

    def on_add(self, r):
        r.user.get_extended_profile().increase_kudos(250)
        return {'_redirect':'/accounts/profile/'}

    def on_delete(self, r):
        r.user.get_extended_profile().decrease_kudos(250)
        return {'_redirect':'/accounts/profile/'}

    def on_view(self, r):
        return {'_redirect': self.owner.get_view_url()}

    class WIDEIO_Meta:
        NO_DRAFT = True
        CAN_TRANSFER=False
        DISABLE_VIEW_ACCOUNTING = True
        permissions = dec.all_permissions_from_request_rwa(
            lambda r: True,
            lambda r: r.user is not None,
            lambda r: r.user is not None)
        icon = "ion-university"
        MOCKS={
            'default':[
                {
                    'organisation': 'Camford University',
                    'cursus': """
# Physics
        - Photonics
        - Electronics
        - Magnetism
        - Thermodynamic
                    """
                }
            ]
        }

    @staticmethod
    def can_list(request):
        return request.user.is_staff
