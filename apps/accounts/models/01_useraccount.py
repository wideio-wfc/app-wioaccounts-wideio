#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#ted in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# This program is distribu
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import settings
import urllib
import base64
import random

from django.contrib.auth.models import AbstractUser
from django.template.loader import render_to_string

from wioframework.amodels import *
from references.models import Image
from wioframework import decorators as dec


@wideio_relaction("accounts.UserAccount",
    possible=lambda o, r: (o.is_scientist == False and r.user.is_superuser))
def set_scientist(self, request):
    self.is_scientist = True
    self.is_customer = False
    self.save()
    return "alert('Done');"

@wideio_relaction("accounts.UserAccount",
    possible=lambda o, r: (o.is_customer == False and r.user.is_superuser))
def set_business(self, request):
    self.is_scientist = False
    self.is_customer = True
    self.save()
    return "alert('Done');"


@wideio_relaction("accounts.UserAccount",
    possible=lambda o, r: (
        o.is_investor == False and r.user.is_superuser))
def set_investor(self, request):
    self.is_investor=True
    self.save()
    return "alert('Done');"

@wideio_relaction("accounts.UserAccount",
    possible=lambda o, r: (
        o.is_verified_scientist == False and r.user.is_superuser))
def set_verified_scientist(self, request):
    self.is_verified_scientist = True
    self.save()
    return "alert('Done');"

@wideio_relaction("accounts.UserAccount",
    possible=lambda o, r: (
        o.is_verified_scientist and r.user.is_superuser))
def unset_verified_scientist(self, request):
    self.is_verified_scientist = False
    self.save()
    return "alert('Done');"
